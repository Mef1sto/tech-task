package com.align.techtask.controller;

import com.align.techtask.controller.error.CommonExceptionHandler;
import com.align.techtask.requestbody.UserCredentials;
import com.align.techtask.responsebody.TokenResponse;
import com.align.techtask.security.JwtTokenUtil;
import com.align.techtask.service.UserDetailsServiceExt;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.web.util.UriComponentsBuilder.fromPath;

@ExtendWith(MockitoExtension.class)
class AuthControllerTest {

    @Mock
    private AuthenticationManager manager;
    @Mock
    private JwtTokenUtil tokenUtil;
    @Mock
    private UserDetailsServiceExt userDetailsService;
    @InjectMocks
    private AuthController controller;

    private ObjectMapper objMapper = new ObjectMapper();
    private MockMvc mvc;

    private UserCredentials userCredentials;
    private final String
            username = "user",
            password = "password",
            token = "eyJhbGciOiJIUzUxMiJ9.eyJzY29wZXMiOiJST";

    @BeforeEach
    void setUp() {
        userCredentials = new UserCredentials(username, password);

        mvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(new CommonExceptionHandler())
                .build();
    }

    @Test
    void test_generateToken() throws Exception {
        doReturn(token).when(tokenUtil).generateToken(any());

        mvc.perform(
                MockMvcRequestBuilders
                        .post(fromPath("/auth/token").toUriString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objMapper.writeValueAsString(userCredentials))
        ).andExpect(status().isOk())
                .andExpect(
                        content()
                                .json(objMapper.writeValueAsString(new TokenResponse(token))));
    }

}
