package com.align.techtask.controller;

import com.align.techtask.controller.error.CommonExceptionHandler;
import com.align.techtask.entity.Product;
import com.align.techtask.exceptions.CommonException;
import com.align.techtask.requestbody.SearchProductsRequest;
import com.align.techtask.requestbody.UpdateProductRequest;
import com.align.techtask.service.ProductsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.web.util.UriComponentsBuilder.fromPath;

@ExtendWith(MockitoExtension.class)
class ProductsControllerTest {

    private Product product1;
    private Product invalidProduct;
    private Pageable pageable;
    private int page = 0, size = 10;

    @Mock
    private ProductsService productsService;
    private ProductsController controller;

    private ObjectMapper objMapper = new ObjectMapper();
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        product1 = new Product();
        product1.setId(UUID.randomUUID());
        product1.setName("product1");
        product1.setBrand("brand1");
        product1.setPrice(new BigDecimal("100.1"));
        product1.setQuantity(5);

        invalidProduct = new Product();
        invalidProduct.setQuantity(-5);

        pageable = PageRequest.of(page, size);
        controller = new ProductsController(productsService);

        mvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(new CommonExceptionHandler())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void test_createProduct() throws Exception {
        when(productsService.createProduct(any())).thenAnswer(invocation -> invocation.getArgument(0));
        mvc.perform(
                MockMvcRequestBuilders
                        .post(fromPath("/products").toUriString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objMapper.writeValueAsString(product1))
        ).andExpect(status().isCreated());
    }

    @Test
    void test_createInvalidProduct() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .post(fromPath("/products").toUriString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objMapper.writeValueAsString(invalidProduct))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void test_getProduct() throws Exception {
        when(productsService.getProduct(any(UUID.class))).thenReturn(product1);
        mvc.perform(
                MockMvcRequestBuilders
                        .get(fromPath("/products/" + UUID.randomUUID()).toUriString())
        ).andExpect(status().isOk())
                .andExpect(content().json(objMapper.writeValueAsString(product1)));
    }


    @Test
    void test_getProductNotFound() throws Exception {
        CommonException notFoundEx = new CommonException("Not Found", HttpStatus.NOT_FOUND.value());
        when(productsService.getProduct(any(UUID.class))).thenThrow(notFoundEx);
        mvc.perform(
                MockMvcRequestBuilders
                        .get(fromPath("/products/" + UUID.randomUUID()).toUriString())
        ).andExpect(status().isNotFound());
    }

    @Test
    void test_getAllProducts() throws Exception {
        Page<Product> productPage = new PageImpl<>(Collections.singletonList(product1));
        when(productsService.getProducts(eq(pageable))).thenReturn(productPage);
        mvc.perform(
                MockMvcRequestBuilders
                        .get(fromPath("/products").toUriString())
                        .param("size", String.valueOf(size))
                        .param("page", String.valueOf(page))
        ).andExpect(status().isOk())
                .andExpect(content().json(objMapper.writeValueAsString(productPage)));
    }

    @Test
    void test_searchProducts() throws Exception {
        Page<Product> productPage = new PageImpl<>(Collections.singletonList(product1));
        SearchProductsRequest searchReq = new SearchProductsRequest();
        searchReq.setName(product1.getName());

        when(productsService.searchProducts(eq(pageable), eq(searchReq.createExample()))).thenReturn(productPage);
        mvc.perform(
                MockMvcRequestBuilders
                        .post(fromPath("/products/search").toUriString())
                        .param("size", String.valueOf(size))
                        .param("page", String.valueOf(page))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objMapper.writeValueAsString(searchReq))
        ).andExpect(status().isOk())
                .andExpect(content().json(objMapper.writeValueAsString(productPage)));
    }

    @Test
    void test_patchProduct() throws Exception {
        UpdateProductRequest updateReq = new UpdateProductRequest();
        updateReq.setName("newName");
        updateReq.setBrand("newBrand");
        when(productsService.updateProduct(any(UUID.class), eq(updateReq))).thenReturn(product1);
        mvc.perform(
                MockMvcRequestBuilders
                        .patch(fromPath("/products/" + UUID.randomUUID()).toUriString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objMapper.writeValueAsString(updateReq))
        ).andExpect(status().isOk())
                .andExpect(content().json(objMapper.writeValueAsString(product1)));
    }

    @Test
    void test_patchProductIncorrect() throws Exception {
        UpdateProductRequest updateReq = new UpdateProductRequest();
        updateReq.setPrice(new BigDecimal("-5"));
        mvc.perform(
                MockMvcRequestBuilders
                        .patch(fromPath("/products/" + UUID.randomUUID()).toUriString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objMapper.writeValueAsString(updateReq))
        ).andExpect(status().isBadRequest());
    }
}
