package com.align.techtask;

import com.align.techtask.requestbody.SearchProductsRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static com.align.techtask.controller.ApiPaths.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.web.util.UriComponentsBuilder.fromPath;

@SpringBootTest
class TechTaskApplicationTests {

    @Autowired
    WebApplicationContext context;
    ObjectMapper objectMapper;

    MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();

        objectMapper = context.getBean(ObjectMapper.class);
    }

    @Test
    void contextLoads() {
    }

    @Test
    void test_getProductShouldReturnNotAuth() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .get(fromPath(PRODUCTS_PATH + UUID.randomUUID()).toUriString())
        ).andExpect(status().isUnauthorized());
    }

    @Test
    void test_createProductShouldReturnNotAuth() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .post(fromPath(PRODUCTS_PATH).toUriString())
                        .contentType(APPLICATION_JSON)
                        .content(anyString())
        ).andExpect(status().isUnauthorized());

    }

    @Test
    void test_deleteProductShouldReturnNotAuth() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .delete(fromPath(PRODUCTS_PATH).toUriString())
        ).andExpect(status().isUnauthorized());
    }

    @Test
    void test_getStockShouldReturnNotAuth() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .get(fromPath(PRODUCTS_PATH + STOCK_PATH).toUriString())
        ).andExpect(status().isUnauthorized());

    }

    @Test
    void test_patchProductShouldReturnNotAuth() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .patch(fromPath(PRODUCTS_PATH).toUriString())
                        .contentType(APPLICATION_JSON)
                        .content(anyString())
        ).andExpect(status().isUnauthorized());

    }

    @Test
    void test_getAllProductsShouldReturnNotAuth() throws Exception {
        mvc.perform(
                MockMvcRequestBuilders
                        .get(fromPath(PRODUCTS_PATH).toUriString())
        ).andExpect(status().isUnauthorized());

    }

    @Test
    void test_searchProductsShouldReturnOk() throws Exception {
        SearchProductsRequest request = new SearchProductsRequest();
        request.setName(anyString());
        mvc.perform(
                MockMvcRequestBuilders
                        .post(fromPath(PRODUCTS_PATH + SEARCH_PATH).toUriString())
                        .contentType(APPLICATION_JSON)
                        .content(
                                objectMapper.writeValueAsString(request)
                        )
        ).andExpect(status().isOk());
    }
}
