package com.align.techtask.service;

import com.align.techtask.entity.Product;
import com.align.techtask.exceptions.CommonException;
import com.align.techtask.repository.ProductsRepository;
import com.align.techtask.requestbody.SearchProductsRequest;
import com.align.techtask.requestbody.UpdateProductRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.domain.*;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductsRepository repository;
    private RetryTemplate retryTemplate;
    private ProductsService productsService;

    private Product product;
    private UUID productId = UUID.fromString("1097992d-8644-4db4-a55c-45b935eba9f1");

    @BeforeEach
    void setUp() throws CommonException {
        product = new Product();
        product.setId(productId);
        product.setQuantity(1);
        product.setPrice(new BigDecimal(1));
        product.setBrand("brand");
        product.setName("name");

        Mockito.lenient().doCallRealMethod().when(repository).mustFindById(any());
        SimpleRetryPolicy policy = new SimpleRetryPolicy(5,
                singletonMap(OptimisticLockingFailureException.class, true),
                false);
        retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(policy);
        retryTemplate.setThrowLastExceptionOnExhausted(true);
        Mockito.lenient().when(repository.save(any())).then(invocation -> invocation.getArgument(0));
        productsService = new ProductsServiceImpl(repository, retryTemplate);
    }

    @Test
    void test_removeProduct() throws CommonException {
        when(repository.findById(productId)).thenReturn(Optional.of(product));
        productsService.removeProduct(productId);
        verify(repository, times(1)).mustFindById(productId);
        verify(repository, times(1)).delete(product);
    }

    @Test
    void test_removeProductThrowException() throws CommonException {
        when(repository.findById(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(CommonException.class, () -> productsService.removeProduct(productId));
        verify(repository, times(1)).mustFindById(any());
        verify(repository, times(0)).delete(any());
    }

    @Test
    void test_getProduct() throws CommonException {
        when(repository.findById(productId)).thenReturn(Optional.of(product));
        Product actual = productsService.getProduct(productId);
        assertEquals(product, actual);
        verify(repository, times(1)).mustFindById(productId);
        verify(repository, times(1)).findById(productId);
    }

    @Test
    void test_getProductThrowException() throws CommonException {
        when(repository.findById(any())).thenReturn(Optional.empty());
        assertThrows(CommonException.class, () -> productsService.getProduct(productId));
        verify(repository, times(1)).mustFindById(any());
        verify(repository, times(1)).findById(any());
    }

    @Test
    void test_createProduct() {
        Product created = productsService.createProduct(product);
        assertEquals(product, created);
    }

    @Test
    void test_updateProduct() throws CommonException {
        when(repository.findById(productId)).thenReturn(Optional.of(product));
        UpdateProductRequest updReq = new UpdateProductRequest();
        updReq.setName("newName");
        updReq.setBrand("newBrand");
        updReq.setPrice(new BigDecimal("2"));
        updReq.setQuantity(2);

        Product updated = productsService.updateProduct(productId, updReq);
        verify(repository).save(eq(updated));
        assertEquals(updReq.getName(), updated.getName());
        assertEquals(updReq.getBrand(), updated.getBrand());
        assertEquals(updReq.getPrice(), updated.getPrice());
        assertEquals(updReq.getQuantity(), updated.getQuantity());
    }

    @Test
    void test_updateProductEmptyName() throws CommonException {
        String oldName = product.getName();
        when(repository.findById(productId)).thenReturn(Optional.of(product));
        UpdateProductRequest updReq = new UpdateProductRequest();
        updReq.setName("");

        Product updated = productsService.updateProduct(productId, updReq);
        verify(repository).save(eq(updated));
        assertNotEquals(updReq.getName(), updated.getName());
        assertEquals(oldName, updated.getName());
    }

    @Test
    void test_updateProductOptimisticLock() throws CommonException {
        when(repository.save(product))
                .thenThrow(new OptimisticLockingFailureException("Test"))
                .thenReturn(product);
        when(repository.findById(productId)).thenReturn(Optional.of(product));

        UpdateProductRequest updReq = new UpdateProductRequest();
        updReq.setName("anotherName");

        Product updated = productsService.updateProduct(productId, updReq);
        verify(repository, times(2)).save(eq(updated));
        assertEquals(updReq.getName(), updated.getName());
    }

    @Test
    void test_findAllProducts() {
        Pageable pageRequest = PageRequest.of(1, 10);
        Page<Product> page = new PageImpl<>(singletonList(product));

        when(repository.findAll(pageRequest)).thenReturn(page);
        Page<Product> products = productsService.getProducts(pageRequest);
        verify(repository).findAll(eq(pageRequest));
        assertEquals(page, products);
    }

    @Test
    void test_getStockProductsLessThanEqual() {
        Pageable pageRequest = PageRequest.of(1, 10);
        Page<Product> page = new PageImpl<>(singletonList(product));
        int quantity = 1;

        when(repository.findByQuantityLessThanEqual(quantity, pageRequest)).thenReturn(page);
        Page<Product> products = productsService.getStockProductsLessThanEqual(quantity, pageRequest);
        verify(repository).findByQuantityLessThanEqual(eq(quantity), eq(pageRequest));
        assertEquals(page, products);
    }

    @Test
    void test_searchProducts() {
        Pageable pageRequest = PageRequest.of(1, 10);
        Page<Product> page = new PageImpl<>(singletonList(product));
        SearchProductsRequest searchReq = new SearchProductsRequest();
        searchReq.setName(product.getName());
        Example<Product> example = searchReq.createExample();
        when(repository.findAll(example, pageRequest)).thenReturn(page);
        Page<Product> products = productsService.searchProducts(pageRequest, example);
        verify(repository).findAll(eq(example), eq(pageRequest));
        assertEquals(page, products);
    }

}
