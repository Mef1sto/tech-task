package com.align.techtask.service;


import com.align.techtask.entity.User;
import com.align.techtask.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class UserDetailsServiceTest {

    @Mock
    UserRepository repository;
    @InjectMocks
    UserDetailsServiceExtImpl userDetailsService;

    @Test
    void test_loadUser() {
        User user = new User("name", "password", Collections.singleton("ADMIN"));
        Set<String> expected = user.getRoles().stream().map(role -> "ROLE_" + role).collect(toSet());

        doReturn(Optional.of(user)).when(repository).findByUsername(any());

        UserDetails userDetails = userDetailsService.loadUserByUsername(anyString());

        Set<String> actual = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).collect(toSet());
        assertEquals(user.getUsername(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
        assertEquals(expected, actual);
    }

    @Test
    void test_loadUserThrowException() {
        doReturn(Optional.empty()).when(repository).findByUsername(any());
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(anyString()));
    }
}
