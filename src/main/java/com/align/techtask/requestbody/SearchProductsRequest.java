package com.align.techtask.requestbody;

import com.align.techtask.entity.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import java.math.BigDecimal;
import java.util.Objects;

@ApiModel(description = "Model for search products request.")
public class SearchProductsRequest {
    @ApiModelProperty("Contain specific text in name")
    private String name;
    @ApiModelProperty("Contain specific text in brand")
    private String brand;
    @ApiModelProperty("Equals specific price")
    private BigDecimal price;

    public SearchProductsRequest() {
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Example<Product> createExample() {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreNullValues()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        Product product = new Product(this.name, this.brand, this.price, null);
        return Example.of(product, matcher);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchProductsRequest)) return false;
        SearchProductsRequest that = (SearchProductsRequest) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, brand, price);
    }

    @Override
    public String toString() {
        return "SearchProductsRequest{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                '}';
    }
}
