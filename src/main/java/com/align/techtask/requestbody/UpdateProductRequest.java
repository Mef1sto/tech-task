package com.align.techtask.requestbody;

import com.align.techtask.entity.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Objects;

@ApiModel(description = "Model for update product request.")
public class UpdateProductRequest {
    @ApiModelProperty("Changed name")
    private String name;
    @ApiModelProperty("Changed brand")
    private String brand;
    @ApiModelProperty("Changed price(should be positive)")
    @Positive
    private BigDecimal price;
    @ApiModelProperty("Changed quantity(should be positive or zero)")
    @PositiveOrZero
    private Integer quantity;

    public UpdateProductRequest() {
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void propagateTo(Product original) {
        if (!StringUtils.isEmpty(this.name))
            original.setName(this.name);
        if (!StringUtils.isEmpty(this.brand))
            original.setBrand(this.brand);
        if (this.price != null)
            original.setPrice(this.price);
        if (this.quantity != null)
            original.setQuantity(this.quantity);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateProductRequest)) return false;
        UpdateProductRequest that = (UpdateProductRequest) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(price, that.price) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, brand, price, quantity);
    }

    @Override
    public String toString() {
        return "UpdateProductRequest{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
