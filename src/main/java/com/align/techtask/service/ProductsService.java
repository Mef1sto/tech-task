package com.align.techtask.service;

import com.align.techtask.entity.Product;
import com.align.techtask.exceptions.CommonException;
import com.align.techtask.requestbody.UpdateProductRequest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface ProductsService {

    Product createProduct(Product product);

    void removeProduct(@NotNull UUID id) throws CommonException;

    Product getProduct(@NotNull UUID id) throws CommonException;

    Product updateProduct(UUID id, UpdateProductRequest updateProductRequest) throws CommonException;

    Page<Product> getProducts(Pageable page);

    Page<Product> searchProducts(Pageable page, Example<Product> example);

    Page<Product> getStockProductsLessThanEqual(int quantity, Pageable pageable);
}
