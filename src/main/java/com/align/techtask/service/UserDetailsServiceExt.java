package com.align.techtask.service;

import com.align.techtask.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserDetailsServiceExt extends UserDetailsService {
    void saveUser(User user);
}