package com.align.techtask.service;

import com.align.techtask.entity.Product;
import com.align.techtask.exceptions.CommonException;
import com.align.techtask.repository.ProductsRepository;
import com.align.techtask.requestbody.UpdateProductRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.UUID;

import static com.align.techtask.config.MainConfig.OPTIMISTIC_LOCK_RETRY_TEMPLATE;

@Service
public class ProductsServiceImpl implements ProductsService {

    private final static Logger log = LoggerFactory.getLogger(ProductsServiceImpl.class);

    private final ProductsRepository repository;
    private final RetryTemplate retryTemplate;

    @Autowired
    public ProductsServiceImpl(ProductsRepository repository,
                               @Qualifier(OPTIMISTIC_LOCK_RETRY_TEMPLATE) RetryTemplate retryTemplate) {
        this.repository = repository;
        this.retryTemplate = retryTemplate;
    }

    @Override
    public Product createProduct(Product product) {
        return repository.save(product);
    }

    @Override
    public void removeProduct(@NotNull UUID id) throws CommonException {
        Product product = repository.mustFindById(id);
        log.debug("Product for deletion was found: {}", product);
        repository.delete(product);
    }

    @Override
    public Product getProduct(@NotNull UUID id) throws CommonException {
        return repository.mustFindById(id);
    }

    @Override
    public Product updateProduct(UUID id, UpdateProductRequest updateProductRequest) throws CommonException {
        return retryTemplate.execute(ctx -> {
            Product productToUpdate = repository.mustFindById(id);
            log.debug("Found product: {}. Start propagation", productToUpdate);
            updateProductRequest.propagateTo(productToUpdate);
            Product savedProduct = repository.save(productToUpdate);
            log.debug("Propagation finished: {}", savedProduct);
            return savedProduct;
        });
    }

    @Override
    public Page<Product> getProducts(Pageable page) {
        return repository.findAll(page);
    }

    @Override
    public Page<Product> searchProducts(Pageable page, Example<Product> example) {
        return repository.findAll(example, page);
    }

    @Override
    public Page<Product> getStockProductsLessThanEqual(int quantity, Pageable pageable) {
        return repository.findByQuantityLessThanEqual(quantity, pageable);
    }
}
