package com.align.techtask.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import static java.util.Collections.singletonMap;

@Configuration
public class MainConfig {

    public static final String OPTIMISTIC_LOCK_RETRY_TEMPLATE = "optimisticLockRetryTemplate";

    @Bean(OPTIMISTIC_LOCK_RETRY_TEMPLATE)
    public RetryTemplate optimisticLockRetryTemplate() {
        SimpleRetryPolicy policy = new SimpleRetryPolicy(5,
                singletonMap(OptimisticLockingFailureException.class, true),
                false);
        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(policy);
        retryTemplate.setThrowLastExceptionOnExhausted(true);
        return retryTemplate;
    }
}
