package com.align.techtask.controller;

import com.align.techtask.config.ApiPageable;
import com.align.techtask.entity.Product;
import com.align.techtask.exceptions.CommonException;
import com.align.techtask.requestbody.SearchProductsRequest;
import com.align.techtask.requestbody.UpdateProductRequest;
import com.align.techtask.responsebody.ErrorResponse;
import com.align.techtask.service.ProductsService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

import static com.align.techtask.controller.ApiPaths.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Api(tags = {"Products"})
@RestController
@RequestMapping(path = PRODUCTS_PATH, produces = APPLICATION_JSON_VALUE)
public class ProductsController {

    private static final Logger log = LoggerFactory.getLogger(ProductsController.class);

    private final ProductsService productsService;

    @Autowired
    public ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }


    @ApiOperation(
            value = "Create new product.",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "Product successfully created"),
            @ApiResponse(code = 400, message = "Product entity is not valid", response = ErrorResponse.class)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> saveProduct(
            @ApiParam(value = "Product entity to create", required = true)
            @Valid @RequestBody Product product) {
        log.info("Request to create product: {}", product);
        Product savedProduct = productsService.createProduct(product);
        log.info("Product was successfully created: id = {}", savedProduct.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
    }

    @ApiOperation(
            value = "Delete product.",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "Product successfully deleted"),
            @ApiResponse(code = 404, message = "Product with specified Id is not found", response = ErrorResponse.class)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(path = ID_PATH, method = DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeProduct(
            @ApiParam(value = "Id of product you want to delete", required = true)
            @PathVariable("id") UUID id) throws CommonException {
        log.info("Request to delete product {}", id);
        productsService.removeProduct(id);
        log.info("Product {} was deleted", id);
    }

    @ApiOperation(
            value = "Get specific product.",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Product successfully found"),
            @ApiResponse(code = 404, message = "Product not found by specified id", response = ErrorResponse.class)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(path = ID_PATH, method = GET)
    public ResponseEntity<Product> getProduct(
            @ApiParam(value = "Id of product you want to get", required = true)
            @PathVariable("id") UUID id) throws CommonException {
        log.info("Get product with id {}", id);
        Product product = productsService.getProduct(id);
        log.info("Got product {}", product);
        return ResponseEntity.ok(product);
    }

    @ApiOperation(
            value = "Get the rest of products.",
            notes = "The result is pageable and includes the upper border",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Products successfully found")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @ApiPageable
    @RequestMapping(path = STOCK_PATH, method = GET)
    public Page<Product> getStock(
            @ApiParam(value = "Upper border of the quantity to return products", required = true)
            @RequestParam(value = "less")
            int quantity,
            Pageable pageable) {
        log.info("Request to get the rest less than {}", quantity);
        return productsService.getStockProductsLessThanEqual(quantity, pageable);
    }

    @ApiOperation(
            value = "Update product fields",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Product successfully updated"),
            @ApiResponse(code = 404, message = "Product not found by specified id", response = ErrorResponse.class)
    })
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(path = ID_PATH, method = PATCH)
    public ResponseEntity<Product> patchProduct(
            @ApiParam(value = "Id of product you want to get", required = true)
            @PathVariable("id") UUID id,
            @ApiParam(value = "Update product body", required = true)
            @RequestBody @Valid UpdateProductRequest productRequest) throws CommonException {
        log.info("Request to update product id {} with {}", id, productRequest);
        Product updateProduct = productsService.updateProduct(id, productRequest);
        log.info("Product {} was successfully updated", updateProduct.getId());
        return ResponseEntity.ok(updateProduct);
    }

    @ApiOperation(
            value = "Get all products (Pageable)",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Products successfully found")
    })
    @ApiPageable
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = GET)
    @ResponseStatus(code = HttpStatus.OK)
    public Page<Product> getAllProducts(Pageable page) {
        log.info("Request to get products: {}", page);
        return productsService.getProducts(page);
    }

    @ApiOperation(
            value = "Search products (Pageable)",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Products successfully found")
    })
    @ApiPageable
    @RequestMapping(
            path = SEARCH_PATH,
            method = POST,
            consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.OK)
    public Page<Product> searchProducts(
            Pageable page,
            @ApiParam(value = "Search request body", required = true)
            @RequestBody SearchProductsRequest searchRequest) {
        log.info("Search products: {}, {}", searchRequest, page);
        return productsService.searchProducts(page, searchRequest.createExample());
    }
}
