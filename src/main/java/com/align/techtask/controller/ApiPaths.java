package com.align.techtask.controller;

public interface ApiPaths {

    String AUTH_PATH = "/auth",
            TOKEN_PATH = "/token",
            PRODUCTS_PATH = "/products",
            STOCK_PATH = "/stock",
            SEARCH_PATH = "/search",
            ID_PATH = "/{id}";
}
