package com.align.techtask.controller;

import com.align.techtask.requestbody.UserCredentials;
import com.align.techtask.responsebody.TokenResponse;
import com.align.techtask.security.JwtTokenUtil;
import com.align.techtask.service.UserDetailsServiceExt;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.align.techtask.controller.ApiPaths.AUTH_PATH;
import static com.align.techtask.controller.ApiPaths.TOKEN_PATH;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Api(tags = {"Authentication"})
@RestController
@RequestMapping(path = AUTH_PATH, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class AuthController {

    private final AuthenticationManager manager;
    private final JwtTokenUtil tokenUtil;
    private final UserDetailsService userDetailsService;

    @Autowired
    public AuthController(AuthenticationManager manager,
                          JwtTokenUtil tokenUtil,
                          UserDetailsServiceExt userDetailsService) {
        this.manager = manager;
        this.tokenUtil = tokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @ApiOperation(
            value = "Generate JWT Token",
            produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE,
            response = TokenResponse.class
    )
    @RequestMapping(value = TOKEN_PATH, method = POST)
    public ResponseEntity<TokenResponse> generateToken(
            @ApiParam(value = "Body with user credentials", required = true)
            @RequestBody UserCredentials credentials) throws AuthenticationException {

        manager.authenticate(new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword()));
        UserDetails userDetails = userDetailsService.loadUserByUsername(credentials.getUsername());
        
        final String token = tokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new TokenResponse(token));
    }
}
