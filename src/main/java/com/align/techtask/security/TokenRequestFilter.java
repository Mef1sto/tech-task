package com.align.techtask.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class TokenRequestFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(TokenRequestFilter.class);
    private static final String BEARER = "Bearer ";
    private static final String AUTHORIZATION = "Authorization";

    private final JwtTokenUtil tokenUtil;
    private final UserDetailsService userDetailsService;

    @Autowired
    public TokenRequestFilter(
            JwtTokenUtil tokenUtil,
            @Qualifier("userDetailsService")
            UserDetailsService userDetailsService) {
        this.tokenUtil = tokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String auth = httpServletRequest.getHeader(AUTHORIZATION);

        String username = null, token = null;

        if (auth != null && auth.startsWith(BEARER)) {
            log.debug("{} header: {}", AUTHORIZATION, auth);
            token = auth.substring(BEARER.length());
            username = tokenUtil.getUsernameFromToken(token);
            log.debug("Got username from token - {}", username);
        }
        
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            log.debug("Found userdetails in repository");
            if (tokenUtil.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                log.debug("User {} authenticated successfully", username);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}