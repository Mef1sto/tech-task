package com.align.techtask.responsebody;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.Objects;

@ApiModel(description = "Error response")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    @ApiModelProperty(value = "Status of error")
    private int status;
    @ApiModelProperty(value = "Error message")
    private String message;
    @ApiModelProperty(value = "Body")
    private Object body;
    @ApiModelProperty(value = "Timestamp of the error")
    private Date timestamp;

    public ErrorResponse() {
        this.timestamp = new Date();
    }

    public ErrorResponse(int status, String message, Object body) {
        this.status = status;
        this.message = message;
        this.body = body;
        this.timestamp = new Date();
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getBody() {
        return body;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ErrorResponse)) return false;
        ErrorResponse that = (ErrorResponse) o;
        return status == that.status &&
                Objects.equals(message, that.message) &&
                Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, message, body);
    }


}
