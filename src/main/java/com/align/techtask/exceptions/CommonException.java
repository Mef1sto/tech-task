package com.align.techtask.exceptions;

import java.util.Objects;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class CommonException extends Exception {
    private Object request;
    private int status;

    public CommonException() {
        this.status = INTERNAL_SERVER_ERROR.value();
    }

    public CommonException(Object request, int status) {
        this.request = request;
        this.status = status;
    }

    public CommonException(String message, int status) {
        super(message);
        this.status = status;
    }

    public CommonException(String message, Object request, int status) {
        super(message);
        this.request = request;
        this.status = status;
    }

    public CommonException(String message) {
        super(message);
        this.status = INTERNAL_SERVER_ERROR.value();
    }

    public Object getRequest() {
        return request;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommonException)) return false;
        CommonException that = (CommonException) o;
        return status == that.status &&
                Objects.equals(request, that.request);
    }

    @Override
    public int hashCode() {

        return Objects.hash(request, status);
    }

    @Override
    public String toString() {
        return "CommonException{" +
                "request=" + request +
                ", status=" + status +
                ", message=" + getMessage() +
                '}';
    }
}
