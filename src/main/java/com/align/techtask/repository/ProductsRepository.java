package com.align.techtask.repository;

import com.align.techtask.entity.Product;
import com.align.techtask.exceptions.CommonException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductsRepository extends JpaRepository<Product, UUID> {

    Page<Product> findByQuantityLessThanEqual(int quantity, Pageable pageable);

    default Product mustFindById(UUID id) throws CommonException {
       return findById(id).orElseThrow(
               () -> new CommonException("Product with ID " + id + " not found", HttpStatus.NOT_FOUND.value())
       );
    }
}
